# classification using Perceptron & Hebb algorithm

Using this code, you can create random data for each category according to the mean matrix, covariance matrix, number of data per category and number of epochs, and view the data classification result graphically.
#
more code and text in https://hiddencluster.ir/
#
### run project
1. Within the project directory, create a Python virtual environment by typing:
- `virtualenv myprojectenv`
2. For install our project’s Python requirements, we need to activate the virtual environment. You can do that by typing:
- `source myprojectenv/bin/activate`
- `pip install -r requirements.txt`
3. Finally, you can test your project by starting up the Django development server with this command:
- `python manage.py runserver 127.0.0.1:8000`
4. In your web browser, visit:
- http://127.0.0.1:8000

#
###### MAHDI RAHMANI
