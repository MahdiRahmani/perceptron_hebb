from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import render
import numpy as np
import matplotlib.pyplot as plt


# Create your views here.

class MainView(View):

    def get(self, request):
        print(request)
        return render(request, 'input.html')

    def post(self, request):
        number_of_a_data = int(request.POST['number_of_data_a'])
        a_mean = [float(request.POST['x1_mean_a']), float(request.POST['x2_mean_a'])]
        a_cov = [[float(request.POST['x1_var_a']), float(request.POST['x1x2_cov_a'])],
                 [float(request.POST['x2x1_cov_a']), float(request.POST['x2_var_a'])]]
        a_x1, a_x2 = np.random.multivariate_normal(a_mean, a_cov, number_of_a_data).T
        a_data = []
        for i in range(0, number_of_a_data):
            a_data.append({'x': a_x1[i], 'y': a_x2[i]})

        number_of_b_data = int(request.POST['number_of_data_b'])
        b_mean = [float(request.POST['x1_mean_b']), float(request.POST['x2_mean_b'])]
        b_cov = [[float(request.POST['x1_var_b']), float(request.POST['x1x2_cov_b'])],
                 [float(request.POST['x2x1_cov_b']), float(request.POST['x2_var_b'])]]
        b_x1, b_x2 = np.random.multivariate_normal(b_mean, b_cov, number_of_b_data).T
        b_data = []
        for i in range(0, number_of_b_data):
            b_data.append({'x': b_x1[i], 'y': b_x2[i]})

        epoch = int(request.POST['max_step'])
        w = []
        p_w0, p_w1, p_w2 = 0, 0, 0
        h_bias, h_w1, h_w2 = 0, 0, 0
        p_x0 = 1
        for step in range(0, epoch):
            p_t = -1
            h_y = 1
            for j in range(0, number_of_a_data):
                # Perceptron
                i = p_w0 + p_w1 * a_x1[j] + p_w2 * a_x2[j]
                if i >= 0:
                    y = 1
                else:
                    y = 0
                p_w0 += (p_t - y) * p_x0
                p_w1 += (p_t - y) * a_x1[j]
                p_w2 += (p_t - y) * a_x2[j]

                # Hebb
                if a_x1[j] >= 0:
                    h_x1 = 1
                else:
                    h_x1 = -1
                if a_x2[j] >= 0:
                    h_x2 = 1
                else:
                    h_x2 = -1
                h_w1 += h_x1 * h_y
                h_w2 += h_x2 * h_y
                h_bias += h_y

            p_t = 1
            h_y = -1
            for j in range(0, number_of_b_data):
                # perceptron
                i = p_w0 + p_w1 * b_x1[j] + p_w2 * b_x2[j]
                if i >= 0:
                    y = 1
                else:
                    y = 0
                p_w0 += (p_t - y) * p_x0
                p_w1 += (p_t - y) * b_x1[j]
                p_w2 += (p_t - y) * b_x2[j]

                # Hebb
                if b_x1[j] >= 0:
                    h_x1 = 1
                else:
                    h_x1 = -1
                if b_x2[j] >= 0:
                    h_x2 = 1
                else:
                    h_x2 = -1
                h_w1 += h_x1 * h_y
                h_w2 += h_x2 * h_y
                h_bias += h_y

            min_x1 = min(a_x1.min(), b_x1.min())
            max_x1 = max(a_x1.max(), b_x1.max())

            p_min_x2 = (-1) * ((p_w0 + min_x1 * p_w1) / p_w2)
            p_max_x2 = (-1) * ((p_w0 + max_x1 * p_w1) / p_w2)

            h_min_x2 = (-1) * (((-1) * h_bias + min_x1 * h_w1) / h_w2)
            h_max_x2 = (-1) * (((-1) * h_bias + max_x1 * h_w1) / h_w2)
            w.append({
                'perceptron': {'step': step + 1, 'w0': p_w0, 'w1': p_w1, 'w2': p_w2,
                               'point_data': [{'x': min_x1, 'y': p_min_x2}, {'x': max_x1, 'y': p_max_x2}]},
                'hebb': {'step': step + 1, 'bias': h_bias, 'w1': h_w1, 'w2': h_w2,
                         'point_data': [{'x': min_x1, 'y': h_min_x2}, {'x': max_x1, 'y': h_max_x2}]}
            })

        return render(request, 'output.html', {'a_data': a_data, 'b_data': b_data, 'w': w})
