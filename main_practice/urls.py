from django.urls import path

from .views import (MainView)

app_name = 'main_practice'

urlpatterns = [
    path('input', MainView.as_view(), name='input'),
]
